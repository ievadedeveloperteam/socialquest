//
//  Recorder.h
//  SocialQuest
//
//  Created by Hao He on 13-10-19.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol RecorderDelegate <NSObject>

@required
-(void)refreshList;

@end

@interface Recorder : UIViewController<AVAudioRecorderDelegate>{
    id<RecorderDelegate> delegate;
    IBOutlet UIButton *recordButton;
    IBOutlet UIButton *stopButton;
    IBOutlet UIButton *playButton;
    IBOutlet UILabel *timeLabel;
}

@property(nonatomic,assign,readwrite) id<RecorderDelegate> delegate;

-(IBAction)startRecording:(id)sender;
-(IBAction)stop:(id)sender;
-(IBAction)playRecording:(id)sender;

@end
