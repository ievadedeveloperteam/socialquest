//
//  RecordingList.m
//  SocialQuest
//
//  Created by Hao He on 13-10-22.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//
#import "RecordingList.h"
#import "Recorder.h"
#import "AudioPlayer.h"

@interface RecordingList ()

@end

@implementation RecordingList

NSString *documentsDirectory;
NSMutableArray* filePathsArray;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    //Get file list, reverse, and make mutable copy of it
    filePathsArray=[[[[[[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil] pathsMatchingExtensions:[NSArray arrayWithObject:@"caf"]] reverseObjectEnumerator] allObjects] mutableCopy];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"Record" style:UIBarButtonItemStyleBordered target:self action:@selector(record)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)record{
    Recorder *newRecorder=[[Recorder alloc] initWithNibName:@"Recorder" bundle:nil];
    newRecorder.delegate=self;
    [self.navigationController pushViewController:newRecorder animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [filePathsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    //cell.textLabel.text = [documentsDirectory stringByAppendingPathComponent:[filePathsArray objectAtIndex:indexPath.row]];
    cell.textLabel.text=[filePathsArray objectAtIndex:indexPath.row];

    
    return cell;
}

-(void)refreshList{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    filePathsArray=[[[[[[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil] pathsMatchingExtensions:[NSArray arrayWithObject:@"caf"]] reverseObjectEnumerator] allObjects] mutableCopy];
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        NSError *error = [[NSError alloc] init];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *fileName = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [filePathsArray objectAtIndex:indexPath.row]];
        BOOL success = false;
        
        if ([fileManager isDeletableFileAtPath:fileName]) {
            success = [fileManager removeItemAtPath:fileName error:&error];
        }
        
        if (success) {
            [filePathsArray removeObjectAtIndex:indexPath.row];
            //must be put at the end
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
           
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Can not delete file" message:fileName delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            if (error) {
                NSLog(@"Error: %@", [error localizedDescription]);
            }
        }
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];

    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
     */
    NSString *fileName = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [filePathsArray objectAtIndex:indexPath.row]];
    NSURL *url = [NSURL fileURLWithPath:fileName];
    AudioPlayer* aPlayer=[[AudioPlayer alloc] initWithNibName:@"AudioPlayer" bundle:nil URL:url];
    [self.navigationController pushViewController:aPlayer animated:YES];
}


@end
