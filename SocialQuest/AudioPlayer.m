//
//  AudioPlayer.m
//  SocialQuest
//
//  Created by Hao He on 13-10-23.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "AudioPlayer.h"

@interface AudioPlayer ()

@end

@implementation AudioPlayer

AVAudioPlayer *audioPlayer;
NSTimer* updateTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil URL:(NSURL*)url
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
        NSError* err;
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&err];
        fileNameLabel.text=[url lastPathComponent];
        if(err){
            NSLog(@"Audio Player: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        }
        audioPlayer.numberOfLoops = 0;
        [audioPlayer prepareToPlay];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fileNameLabel.text=[audioPlayer.url lastPathComponent];
    progressSlider.maximumValue=audioPlayer.duration;
    [audioPlayer play];
    updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(update) userInfo:Nil repeats:YES];

}

-(void)update{
    if (audioPlayer.isPlaying) {
        progressSlider.value=audioPlayer.currentTime;
        playButton.titleLabel.text=@"Pause";
    }else{
        playButton.titleLabel.text=@"Play";
        if (updateTimer) {
            [updateTimer invalidate];
        }
    }
}

-(IBAction)stop:(id)sender{
    progressSlider.value=progressSlider.minimumValue;
    audioPlayer.currentTime=0.0;
    if (audioPlayer.isPlaying) {
        [audioPlayer stop];
    }
    playButton.titleLabel.text=@"Play";
}

-(IBAction)play:(id)sender{
    if (audioPlayer.isPlaying) {
        [audioPlayer pause];
        playButton.titleLabel.text=@"Play";
        if(updateTimer){
            [updateTimer invalidate];
        }
    } else {
        [audioPlayer play];
        playButton.titleLabel.text=@"Pause";
        if (updateTimer) {
            [updateTimer invalidate];
        }
        updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(update) userInfo:Nil repeats:YES];
    }
}

-(IBAction)progressSliderMoved:(UISlider*)sender{
    audioPlayer.currentTime=sender.value;
}

-(void)back{
    if (audioPlayer.isPlaying) {
        [audioPlayer stop];
    }
    if (updateTimer) {
        [updateTimer invalidate];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
