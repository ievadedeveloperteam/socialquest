//
//  Recorder.m
//  SocialQuest
//
//  Created by Hao He on 13-10-19.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "Recorder.h"

@interface Recorder ()

@end

@implementation Recorder

@synthesize delegate=_delegate;

NSMutableDictionary *recordSetting;
NSString *audioRecorderFilePath;
AVAudioRecorder *audioRecorder;
AVAudioPlayer *audioPlayer;
NSTimer *updateTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
}

-(void)back{
    if(updateTimer){
        [updateTimer invalidate];
    }
    [self.delegate refreshList];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)startRecording:(id)sender{
    if((audioRecorder&&[audioRecorder isRecording])||(audioPlayer&&[audioPlayer isPlaying])){
        return;
    }
    //NSLog(@"Recording started");
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    NSError *err=nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    
    [audioSession setActive:YES error:&err];
    err=nil;
    
    recordSetting=[[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    [recordSetting setValue :[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
    [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];

    // Create a new dated file
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0];
    NSString *caldate = [now description];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    audioRecorderFilePath = [NSString stringWithFormat:@"%@/%@.caf", documentsDirectory, caldate];
    
    NSURL *url = [NSURL fileURLWithPath:audioRecorderFilePath];
    err = nil;
    audioRecorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!audioRecorder){
        NSLog(@"audioRecorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    //prepare to record
    [audioRecorder setDelegate:self];
    [audioRecorder prepareToRecord];
    audioRecorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        return;
    }
    
    // start recording
    //[audioRecorder recordForDuration:(NSTimeInterval) 60];
    [audioRecorder record];
    updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(update) userInfo:nil repeats:YES];
}

-(IBAction)stop:(id)sender{
    if(updateTimer){
        [updateTimer invalidate];
    }
    if (audioRecorder&&[audioRecorder isRecording]) {
        [audioRecorder stop];
        /*
        NSURL *url = [NSURL fileURLWithPath: audioRecorderFilePath];
        NSError *err = nil;
        NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
        if(!audioData){
            NSLog(@"audio data: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        }
        
        //[editedObject setValue:[NSData dataWithContentsOfURL:url] forKey:editedFieldKey];
        
        //[recorder deleteRecording];
        
        
        NSFileManager *fm = [NSFileManager defaultManager];
        
        err = nil;
        [fm removeItemAtPath:[url path] error:&err];
        if(err){
            NSLog(@"File Manager: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        }
        */
        //NSLog(@"Stopped");
    }
    if (audioPlayer&&[audioPlayer isPlaying]) {
        [audioPlayer stop];
        //NSLog(@"Stopped");
    }
    
}

-(IBAction)playRecording:(id)sender{
    // Init audio with playback capability
    if((audioRecorder&&[audioRecorder isRecording])||(audioPlayer&&[audioPlayer isPlaying])){
        return;
    }
    //NSLog(@"Playback started");
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSURL *url = [NSURL fileURLWithPath:audioRecorderFilePath];
    NSError *err;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&err];
    if(err){
        NSLog(@"Audio Player: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
    }
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(update) userInfo:nil repeats:YES];
}

-(void)update{
    if (audioRecorder.isRecording) {
        NSTimeInterval timeInterval=audioRecorder.currentTime;
        int minute=timeInterval/60.0;
        timeLabel.text=[NSString stringWithFormat:@"%02u:%02u",minute,(int)(timeInterval-minute*60.0)];
    }else if (audioPlayer.isPlaying){
        NSTimeInterval timeInterval=audioPlayer.currentTime;
        int minute=timeInterval/60.0;
        timeLabel.text=[NSString stringWithFormat:@"%02u:%02u",minute,(int)(timeInterval-minute*60.0)];
    }else{
        if(updateTimer){
            [updateTimer invalidate];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
