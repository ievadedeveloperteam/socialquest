//
//  AudioPlayer.h
//  SocialQuest
//
//  Created by Hao He on 13-10-23.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioPlayer : UIViewController<AVAudioPlayerDelegate>{
    IBOutlet UIButton *stopButton;
    IBOutlet UIButton *playButton;
    IBOutlet UISlider *progressSlider;
    IBOutlet UILabel *fileNameLabel;
}

-(IBAction)stop:(id)sender;
-(IBAction)play:(id)sender;
-(IBAction)progressSliderMoved:(UISlider*)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil URL:(NSURL*)url;

@end
