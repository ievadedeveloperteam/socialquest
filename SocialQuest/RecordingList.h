//
//  RecordingList.h
//  SocialQuest
//
//  Created by Hao He on 13-10-22.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recorder.h"

@interface RecordingList : UITableViewController<RecorderDelegate>{
}

@end
